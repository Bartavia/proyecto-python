#Crear el diagrama de flujo que proporcione el volumen de un cilindro dados su
#altura y diámetro.

Altura = int(input("Ingresar altura del cilindro : "))
Diametro = int(input("Ingresar diametro de un cilindro : "))
Pi = 3.14

Radio = (Diametro/2)

Volumen = Pi*(pow(Radio,2)*Altura)

print("EL volumen de un cilindro es : " ,Volumen)
