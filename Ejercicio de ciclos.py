#comienso de generar mensaje de entrada
MensajeEntrada = "programa de generacion de tablas de multiplicar".capitalize()
print(MensajeEntrada.center(60,"="))

# opcion=1 es para responder con numeros
#opcion=1
# opcion es para responder con letras
opcion = 's'
#opcion!=0:
while opcion!='n' or opcion!='N':
#chr(27) es para combiar de color el texto
    tabinicio=int(input(chr(27) + "[1;36m" + "Ingrese la tabla Inicial :"))
    tabfinal = int(input("Ingresa la tabla Final :"))

#se ingres con cual tabla iniciar y hasta donde llegar
    while tabfinal<tabinicio:
        print("La tabla de inicio debe ser menor")
        tabinicio = int(input("Ingrese la tabla Inicial :"))
        tabfinal = int(input("Ingresa la tabla Final :"))

#se ingresa el inicio y final de los numeros por los que se multiplica (Rango)
    rangoinicio=int(input("Ingrese el rango Inicial : "))
    rangofinal = int(input("ingrese el rango Final : "))
#while iniciar ciclo
    while tabinicio<tabfinal:
        # para poder pasar algun rango que no queramos
        for i in range(tabinicio,tabfinal+1):
            if i==4:
                print("La tabla {0} no la omprimo porque no quiero ".format(i))
                #continue (funcion de continuar)
                #pass pasa sin evaluar el reseltado
                pass
            #para no
            for j in range(rangoinicio,rangofinal+1):
                if j==5:
                    print("Mas de 5 no quiero: ")
                    break
                resultado=i*j
                print("Multiplicar %d * %d es igual a : %d "%(i,j,resultado))
                print("Multiplicar ",i," * ",j," es igual a : ",resultado)
        tabinicio=tabinicio+1
    opcion=(input("Desea ejecutar nuevamente el proceso : \n"
                         "s: Continuar \n"
                         "n : Salir \n"))
    if (opcion == 'n') or (opcion == 'N'):
        break
else:
    print("gracias por jugar")